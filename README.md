Role Name
=========

A simple role than setup SSH server for using public keys and add users to sudo/ssh

Requirements
------------

Role Variables
--------------

users_list set
    - username: name
      userkey: 
        - one pub key in a row
        - another pub key in a row
      userstate: present | absent

Dependencies
------------



Example Playbook
----------------

play.yml

    - hosts: servers
      roles:
         - { sample.ansible_role_ssh }

License
-------

BSD

Author Information
------------------

Kirill Fedorov
